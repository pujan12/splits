class Item < ApplicationRecord
  belongs_to :boss
  has_many :splits
end
