json.extract! team, :id, :size, :created_at, :updated_at
json.url team_url(team, format: :json)
