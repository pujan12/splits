json.extract! boss, :id, :name, :created_at, :updated_at
json.url boss_url(boss, format: :json)
