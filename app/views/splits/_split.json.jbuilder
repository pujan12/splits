json.extract! split, :id, :item_id, :soldFor, :splitAmt, :team_id, :created_at, :updated_at
json.url split_url(split, format: :json)
