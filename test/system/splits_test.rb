require "application_system_test_case"

class SplitsTest < ApplicationSystemTestCase
  setup do
    @split = splits(:one)
  end

  test "visiting the index" do
    visit splits_url
    assert_selector "h1", text: "Splits"
  end

  test "creating a Split" do
    visit splits_url
    click_on "New Split"

    fill_in "Item", with: @split.item_id
    fill_in "Soldfor", with: @split.soldFor
    fill_in "Splitamt", with: @split.splitAmt
    fill_in "Team", with: @split.team_id
    click_on "Create Split"

    assert_text "Split was successfully created"
    click_on "Back"
  end

  test "updating a Split" do
    visit splits_url
    click_on "Edit", match: :first

    fill_in "Item", with: @split.item_id
    fill_in "Soldfor", with: @split.soldFor
    fill_in "Splitamt", with: @split.splitAmt
    fill_in "Team", with: @split.team_id
    click_on "Update Split"

    assert_text "Split was successfully updated"
    click_on "Back"
  end

  test "destroying a Split" do
    visit splits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Split was successfully destroyed"
  end
end
