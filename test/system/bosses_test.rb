require "application_system_test_case"

class BossesTest < ApplicationSystemTestCase
  setup do
    @boss = bosses(:one)
  end

  test "visiting the index" do
    visit bosses_url
    assert_selector "h1", text: "Bosses"
  end

  test "creating a Boss" do
    visit bosses_url
    click_on "New Boss"

    fill_in "Name", with: @boss.name
    click_on "Create Boss"

    assert_text "Boss was successfully created"
    click_on "Back"
  end

  test "updating a Boss" do
    visit bosses_url
    click_on "Edit", match: :first

    fill_in "Name", with: @boss.name
    click_on "Update Boss"

    assert_text "Boss was successfully updated"
    click_on "Back"
  end

  test "destroying a Boss" do
    visit bosses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Boss was successfully destroyed"
  end
end
