# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
#
i = 1
until i > 5
  Team.create(size: i)
  i += 1
end

#bosses
cox = Boss.create(name: 'Chambers of Xeric')
tob= Boss.create(name: 'Theatre of Blood')
bandos = Boss.create(name: 'General Graardor')
zammy = Boss.create(name: 'Kril Tsutsaroth')
arma = Boss.create(name: 'Kreearra')


#items
arcane = Item.create(name: 'Arcane Prayer Scroll', price: 1200, boss_id: cox.id)
dex = Item.create(name:'Dexterous Prayer Scroll',price: 43000000, boss_id: cox.id)
dinhs = Item.create(name:'Dinhs Bulwark', price: 4000000, boss_id: cox.id)
dragon_claws = Item.create(name: 'Dragon Claws', price: 44000000, boss_id: cox.id)
dhcb = Item.create(name: 'Dragon Hunter Crossbow', price: 120000000, boss_id: cox.id)
elder= Item.create(name:'Elder Maul', price: 10000000, boss_id: cox.id)
kodai= Item.create(name: 'Kodai', price: 77000000, boss_id: cox.id)
twistedbuckler = Item.create(name: 'Twisted Buckler',price: 12000000, boss_id: cox.id)
twistedbow = Item.create(name:'Twisted Bow', price: 11000000000, boss_id: cox.id)


#Theatre of Blood
avernic = Item.create(name: 'Avernic defender Hilt', price: 86310267, boss_id: tob.id)
rapier = Item.create(name: 'Ghrazi Rapier', price: 181186399, boss_id: tob.id)
sangstaff= Item.create(name: 'Sanguinesti staff (uncharged)', price: 119946303, boss_id: tob.id)
justface = Item.create(name: 'Justiciar faceguard', price: 31400529, boss_id: tob.id)
justleg = Item.create(name: 'Justiciar legguards', price: 18708663, boss_id: tob.id)
justchest = Item.create(name: 'Justiciar Chestguard', price: 18025392, boss_id: tob.id)
scythe = Item.create(name: 'Scythe of vitur (uncharged)', price: 735086561, boss_id: tob.id)