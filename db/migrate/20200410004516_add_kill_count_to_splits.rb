class AddKillCountToSplits < ActiveRecord::Migration[6.0]
  def change
    add_column :splits, :killCount, :integer
  end
end
