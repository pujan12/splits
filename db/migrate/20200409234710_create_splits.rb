class CreateSplits < ActiveRecord::Migration[6.0]
  def change
    create_table :splits do |t|
      t.references :item, null: false, foreign_key: true
      t.integer :soldFor
      t.integer :splitAmt
      t.references :team, null: false, foreign_key: true

      t.timestamps
    end
  end
end
