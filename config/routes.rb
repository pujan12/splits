Rails.application.routes.draw do
  resources :splits
  resources :items
  resources :bosses
  resources :teams
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
